import TWEEN from '@tweenjs/tween.js';

export class CurtainScene {
    private canvas: HTMLCanvasElement;
    private ctx: CanvasRenderingContext2D;

    private running = false;
    private loading = true;
    private loadingProgress = 0;
    private curtainTween;
    
    private CURTAIN_COLOR = '#9bffaf';
    private LOADING_TIME = 900;
    private CURTAIN_DURATION = 3000;
    private LOADING_RADIUS = 50;

    public onFinished: CallableFunction = () => null;

    constructor(canvas: HTMLCanvasElement) {
        this.canvas = canvas;
        this.update = this.update.bind(this);
        this.init = this.init.bind(this);
    }

    start() {
        this.running = true;
        setTimeout(() => {
            this.loading = false;
            this.curtainTween.start();
        }, this.LOADING_TIME);
        requestAnimationFrame(this.update);
    }

    stop() {
        this.running = false;
    }

    init() {
        this.ctx = this.canvas.getContext('2d');
        this.ctx.canvas.width  = window.innerWidth;
        this.ctx.canvas.height = window.innerHeight;
        this.ctx.fillStyle = this.CURTAIN_COLOR;
        this.ctx.fillRect(0, 0, window.innerWidth, window.innerHeight);

         // Curtain animation
         let curtainProperties = { size: 0 };
         this.curtainTween = new TWEEN.Tween(curtainProperties)
             .to({ size: window.innerWidth }, this.CURTAIN_DURATION)
             .easing(TWEEN.Easing.Quadratic.InOut)
             .onUpdate(() => {
                 this.ctx.clearRect((window.innerWidth / 2) - curtainProperties.size, 0, curtainProperties.size * 2, window.innerHeight);
             })
             .onComplete(() => {
                this.stop();
                this.onFinished();
            });
    }

    accelerateInterpolator(x) {
        return x * x;
    }
    
    decelerateInterpolator(x) {
        return 1 - ((1 - x) * (1 - x));
    }

    drawCircle(circle, progress) {
        this.ctx.beginPath();
        var start = this.accelerateInterpolator(progress) * circle.speed;
        var end = this.decelerateInterpolator(progress) * circle.speed;
        this.ctx.arc(circle.center.x, circle.center.y, circle.radius, (start - 0.5) * Math.PI, (end - 0.5) * Math.PI);
        this.ctx.lineWidth = 3;
        this.ctx.strokeStyle = "white";
        this.ctx.fill();
        this.ctx.stroke();
    }

    update(time) {
        if(this.running) requestAnimationFrame(this.update);
        if(this.loading) {
            this.ctx.fillStyle = this.CURTAIN_COLOR;
            this.ctx.fillRect(0, 0, window.innerWidth, window.innerHeight);

            this.loadingProgress += 0.02;
            if(this.loadingProgress > 1) {
                this.loadingProgress = 0;
            }
            
            this.drawCircle({
                center: {
                    x: window.innerWidth / 2,
                    y: window.innerHeight / 2
                },
                radius: this.LOADING_RADIUS,
                speed: 4
            }, this.loadingProgress);

        }
        TWEEN.update(time);
    }
}
