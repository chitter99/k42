import * as THREE from 'three';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { ObjectScene, StartupDolly } from './objectScene';

export class AssemblyScene extends ObjectScene {
	protected startupDolly: StartupDolly;

	onInit() {
		this.TEXT = 'Call to unity!';
		this.SPHERE_COLOR = 0xFF8080;
	}

	onLoad() {
		// StartupDolly
		this.startupDolly = new StartupDolly(this.camera, this.CAMERA_TARGET_VECTOR);
		this.startupDolly.init();

		// Wolf		
        let gltfloader = new GLTFLoader();
        gltfloader.load('assets/statue.gltf', (gltf) => {
            let statueMaterial = new THREE.MeshBasicMaterial({ wireframe: true, color: 0x00000 });
            let statue = gltf.scenes[0];
            statue.scale.set(.025, .025, .025);
			statue.position.set(0, -11, 0);
			statue.rotateX((Math.PI / 2) * -1);
            statue.traverse(o => {
                if(o instanceof THREE.Mesh) {
                    o.castShadow = true;
                    o.receiveShadow = true;
                    o.material = statueMaterial;
                }
            });
            this.scene.add(statue);
        });
	}

	onUpdate() {
		if(this.startupDolly.finished) {
            this.control.update();
        }
	}	
}
