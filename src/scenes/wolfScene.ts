import * as THREE from 'three';
import TWEEN from '@tweenjs/tween.js';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import { ObjectScene, StartupDolly } from './objectScene';

export class WolfScene extends ObjectScene {
	protected startupDolly: StartupDolly;

	onInit() {
		this.TEXT = 'Let the adventure begin';
	}

	onLoad() {
		// StartupDolly
		this.startupDolly = new StartupDolly(this.camera, this.CAMERA_TARGET_VECTOR);
		this.startupDolly.init();

		// Wolf		
        let gltfloader = new GLTFLoader();
        gltfloader.load('assets/wolf.gltf', (gltf) => {
            let wolfMaterial = new THREE.MeshBasicMaterial({ wireframe: true, color: 0x00000 });
            let wolf = gltf.scenes[0];
            wolf.scale.set(.02, .02, .02);
            wolf.position.set(0, -11, 0);
            wolf.traverse(o => {
                if(o instanceof THREE.Mesh) {
                    o.castShadow = true;
                    o.receiveShadow = true;
                    o.material = wolfMaterial;
                }
            });
            this.scene.add(wolf);
        });
	}

	onUpdate(time) {
		if(this.startupDolly.finished) {
            this.control.update();
        }
	}	
}
