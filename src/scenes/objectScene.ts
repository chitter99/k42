import * as THREE from 'three';
import TWEEN from '@tweenjs/tween.js';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';

export abstract class ObjectScene {
    protected canvas: HTMLCanvasElement;

    protected TEXT = '';
    protected FONT_URL = 'assets/AmaticSCRegular.json';
    protected SPHERE_COLOR = 0x9bffaf;
    protected FLOOR_COLOR = 0xe8e8e8;
    protected BACKGROUND_COLOR = 0xCFCFCC;
    protected CAMERA_TARGET_VECTOR = new THREE.Vector3(2, 0, 0);

    protected renderer;
    protected control;
    protected scene;
    protected camera;

    constructor(canvas: HTMLCanvasElement) {
		this.canvas = canvas;
		
		this.onInit();

        this.update = this.update.bind(this);
        this.init = this.init.bind(this);
    }

    init() {
        let fontLoader = new THREE.FontLoader();
        
        this.scene = new THREE.Scene();
        this.scene.background = new THREE.Color(this.BACKGROUND_COLOR);
        //this.scene.fog = new THREE.Fog(this.BACKGROUND_COLOR, 10, 90);
        
        this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas, antialias: true, alpha: true });
        this.renderer.setSize(window.innerWidth, window.innerHeight);
        this.renderer.setPixelRatio(window.devicePixelRatio);

        // Camera
        this.camera = new THREE.PerspectiveCamera(50, window.innerWidth / window.innerHeight, 0.1, 1000);
        this.camera.position.z = 70;
        this.camera.position.x = 30;
        this.camera.position.y = 30;

        this.control = new OrbitControls(this.camera, this.renderer.domElement);
        /*this.control.minDistance = 20;
        this.control.maxDistance = 50;
        this.control.minPolarAngle = 0;
        this.control.maxPolarAngle = Math.PI * 0.8; 
        this.control.minAzimuthAngle = -Math.PI * 0.2;
        this.control.maxAzimuthAngle = Math.PI * 0.2;*/
        this.control.target = this.CAMERA_TARGET_VECTOR;

        // Directional Light
        let d = 8.25;
        let directionalLight = new THREE.DirectionalLight(0xffffff, 0.54);
        directionalLight.position.set(-8, 12, 8);
        directionalLight.castShadow = true;
        directionalLight.shadow.mapSize = new THREE.Vector2(1024, 1024);
        directionalLight.shadow.camera.near = 0.1;
        directionalLight.shadow.camera.far = 1500;
        directionalLight.shadow.camera.left = d * -1;
        directionalLight.shadow.camera.right = d;
        directionalLight.shadow.camera.top = d;
        directionalLight.shadow.camera.bottom = d * -1;
        this.scene.add(directionalLight);

        // Hemisphere Light
        let hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.61);
        hemiLight.position.set(0, 50, 0);
        this.scene.add(hemiLight);

        // Floor
        let floorGeometry = new THREE.PlaneGeometry(5000, 5000, 1, 1);
        let floorMaterial = new THREE.MeshPhongMaterial({ color: this.BACKGROUND_COLOR, shininess: 0 });

        let floor = new THREE.Mesh(floorGeometry, floorMaterial);
        floor.rotation.x = -0.5 * Math.PI;
        floor.receiveShadow = true;
        floor.position.y = -11;
        //this.scene.add(floor);

        // Green Sphere
        let sphereGeometry = new THREE.SphereGeometry(8, 32, 32);
        let sphereMaterial = new THREE.MeshBasicMaterial({ color: this.SPHERE_COLOR });
        let sphere = new THREE.Mesh(sphereGeometry, sphereMaterial);
        sphere.position.z = -15;
        sphere.position.y = -2.5;
        sphere.position.x = -0.25;
        this.scene.add(sphere);

        // Text
        fontLoader.load(this.FONT_URL, (font) => {
            let textGeometry = new THREE.TextGeometry(this.TEXT, {
                font: font,
                size: 3,
                height: 0.2
            });
            let text = new THREE.Mesh(textGeometry);
            text.position.set(-10, 2, -5);
            this.scene.add(text);
		});
		
		this.onLoad();
    }

    update(time) {
        requestAnimationFrame(this.update);
        TWEEN.update(time);
		this.onUpdate(time);
        this.renderer.render(this.scene, this.camera);
	}

	abstract onInit();
	abstract onLoad();
	abstract onUpdate(time);
}

export class StartupDolly {
	public finished: boolean = false;
    public dolly;

	private camera;
	private targetVector;

	constructor(camera, targetVector) {
		this.camera = camera;
		this.targetVector = targetVector;
	}

	public init() {
		let cameraTweenPosition = { x: 30, y: 30, z: 70 };
        this.dolly = new TWEEN.Tween(cameraTweenPosition)
            .to({ x: 0, y: 0, z: 20 }, 3000)
            .easing(TWEEN.Easing.Quadratic.InOut)
            .onUpdate(() => {
                this.camera.position.z = cameraTweenPosition.z;
                this.camera.position.y = cameraTweenPosition.y;
                this.camera.position.x = cameraTweenPosition.x;
                this.camera.lookAt(this.targetVector);
            })
			.onComplete(() => { this.finished = true; });
		this.dolly.start();
	}
}
