import * as React from 'react';

import { Page, PageSceneType } from './page';


export function Home() {
	return <Page scene={ PageSceneType.WOLF } />;
}
