import * as React from 'react';
import { Box, Heading, Nav, Header, Main, Stack, Video, Footer, Anchor, Layer } from 'grommet';
import { Instagram, Twitter, Mail } from 'grommet-icons';
import styled from 'styled-components';

import { Curtain } from './curtain';
import { HomeScene, AssemblyScene } from './scene';
import { More } from './more';
import { FooterComponent } from './footer';

const FixedHeader = styled(Header)`
    z-index: 100;
    position: absolute;
`;

const FixedFooter = styled(Footer)`
    width: 100%;
    z-index: 100;
    position: absolute;
    bottom: 0;
`;

const Moreplease = styled(More)`
	display: inline-block;
	position: absolute;
	bottom: 16vh;
	left: 50%;
	z-index: 105;
	transform: translate(0, -50%);
`;

const ScenenBox = styled(Box)`
	overflow: hidden;
`;

export enum PageSceneType {
	NONE,
	WOLF,
	STATUE,
}

export namespace PageSceneType {
	export function getScene(scene: PageSceneType) {
		switch(scene) {
			case PageSceneType.WOLF:
				return <HomeScene />			
			case PageSceneType.STATUE:
				return <AssemblyScene />
			default:
				return null;
		}
	}
}

interface PageProps {
	children?: any
	scene?: PageSceneType
}


export function Page({ children, scene }: PageProps) {
	let hasContent = children != null;
	if(scene == null) scene = PageSceneType.NONE;
	return <Box fill gap='medium'>
		<Stack>
			<Box height='100vh'>
				<FixedHeader fill='horizontal'>
					<Heading margin='medium' level={ 3 } color='light-1'>Coming soon</Heading>
					<Nav direction='row' pad='medium'>
						<Anchor href='https://www.instagram.com/k42group' target='_blank'>
							<Instagram size='medium' color='light-1' />
						</Anchor>
						<Anchor href='https://twitter.com/k42group' target='_blank'>
							<Twitter size='medium' color='light-1' />
						</Anchor>
						<Anchor href='mailto:whois@k42.group' target='_blank'>
							<Mail size='medium' color='light-1' />
						</Anchor>
					</Nav>
				</FixedHeader>
				<ScenenBox>
					{ PageSceneType.getScene(scene) }
				</ScenenBox>
				{ hasContent ? <Moreplease href='#content' /> : null }
				<FixedFooter>
					<Box margin={{ left: 'auto', right: 'auto' }}>
						<Heading level={ 4 } color='light-1' textAlign='center'>@ k42 - home of extraordinary book readers</Heading>
					</Box>
				</FixedFooter>
			</Box>			
			<Box height='100vh'>
				<Curtain />
			</Box>
		</Stack>
		{ hasContent ? <Box flex={ false }>
			<Box id='content'>
				{ children }
			</Box>
			<FooterComponent />
		</Box> : null }
	</Box>;
}