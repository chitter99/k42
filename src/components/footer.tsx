import * as React from 'react';
import { Footer, Box, Heading, Anchor } from 'grommet';
import { Instagram, Twitter, Mail } from 'grommet-icons';

export function FooterComponent() {
	return <Footer background='dark-2' fill='horizontal'>
		<Box direction='column' gap='xxsmall' pad='medium' margin={{ left:'auto', right:'auto'}}>
			<Heading level={4}>Find us on</Heading>
			<Box direction='row' gap='small'>
				<Anchor href='https://www.instagram.com/k42group' target='_blank'>
					<Instagram size='medium' color='light-1' />
				</Anchor>
				<Anchor href='https://twitter.com/k42group' target='_blank'>
					<Twitter size='medium' color='light-1' />
				</Anchor>
				<Anchor href='mailto:whois@k42.group' target='_blank'>
					<Mail size='medium' color='light-1' />
				</Anchor>
			</Box>
		</Box>
	</Footer>;
}
