import * as React from 'react';
import styled from 'styled-components';

const ScrollDownPlease = styled.span`
	position: absolute;
	top: 0;
	left: 50%;
	width: 46px;
	height: 46px;
	margin-left: -23px;
	border: 1px solid #fff;
	border-radius: 100%;
	box-sizing: border-box;
	&:after {
		position: absolute;
		top: 50%;
		left: 50%;
		content: '';
		width: 16px;
		height: 16px;
		margin: -12px 0 0 -8px;
		border-left: 1px solid #fff;
		border-bottom: 1px solid #fff;
		-webkit-transform: rotate(-45deg);
		transform: rotate(-45deg);
		box-sizing: border-box;
	}
`;

interface IMoreProps {
	className?: string
	onClick?: () => void
	href?: string
}

export function More({ className, onClick, href }: IMoreProps) {
	return <a className={ className } href={ href } onClick={ onClick }>
		<ScrollDownPlease></ScrollDownPlease>
	</a>;
}
