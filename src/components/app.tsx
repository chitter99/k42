import * as React from 'react';
import { Grommet } from 'grommet';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import { Theme } from '../theme';
import { Home } from './home';
import { CommunityAssemblyPage } from './assembly';

export const App: React.StatelessComponent<{}> = (props) => {
    return <Router>
		<Grommet theme={ Theme } background='dark-1' full>
			<Switch>
				<Route path='/generalversammlung2k20'>
					<CommunityAssemblyPage />
				</Route>
				<Route>
					<Home />
				</Route>
			</Switch>
		</Grommet>
	</Router>;
};
