import * as React from 'react';
import styled from 'styled-components';
import { CurtainScene } from '../scenes/curtainScene';
import { Box } from 'grommet';

const CurtainBox = styled(Box)`
    z-index: 101;
`;

type CurtainState = {
    finished: boolean  
};

export class Curtain extends React.Component<any, CurtainState> {
    private canvasRef;
    private scene;

    constructor(props) {
        super(props);
        this.state = {
            finished: false  
        };
        this.canvasRef = React.createRef();
        this.onFinished = this.onFinished.bind(this);
    }

    componentDidMount() {
        this.scene = new CurtainScene(this.canvasRef.current);
        this.scene.onFinished = this.onFinished;
        this.scene.init();
        this.scene.start();
    }

    onFinished() {
        this.setState({
            finished: true
        });
    }

    render() {
        if(this.state.finished) return null;
        return <CurtainBox fill>
            <canvas ref={ this.canvasRef } />
        </CurtainBox>;
    }
}

