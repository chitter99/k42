import * as React from 'react';
import styled from 'styled-components';
import { Box, Paragraph, Text } from 'grommet';

const DoubleQuote = styled.span`
	position: relative;
	top: 1em;
	left: 0.2em;
	&:before {
		color: #6FFFB0;
		font-family: Arial;
		content: '“';
		font-size: 4em;
	}
`;


export function Quote({ by, text }) {
	return <Box as='blockquote' border={{
		color: 'brand',
		size: 'medium',
		side: 'left'
	}}>
		<Box direction='row' gap='small' justify='start'>
			<Box>
				<DoubleQuote />
			</Box>
			<Box>
				<Paragraph size='medium'>{ text }</Paragraph>
				<Text>{ by }</Text>
			</Box>
		</Box>
	</Box>;
}
