import * as React from 'react';
import { WolfScene } from '../scenes/wolfScene';
import { AssemblyScene as AssScene } from '../scenes/assemblyScene';  

export class HomeScene extends React.Component<any, any> {
    private canvasRef;
    private wolfScene;

    constructor(props) {
        super(props);
        this.canvasRef = React.createRef();
    }

    componentDidMount() {
        this.wolfScene = new WolfScene(this.canvasRef.current);
        this.wolfScene.init();
        this.wolfScene.update();
    }

    render() {
        return <canvas ref={ this.canvasRef } />;
    }
}


export class AssemblyScene extends React.Component<any, any> {
    private canvasRef;
    private assemblyScene;

    constructor(props) {
        super(props);
        this.canvasRef = React.createRef();
    }

    componentDidMount() {
        this.assemblyScene = new AssScene(this.canvasRef.current);
        this.assemblyScene.init();
        this.assemblyScene.update();
    }

    render() {
        return <canvas ref={ this.canvasRef } />;
    }
}

