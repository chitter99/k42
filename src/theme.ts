import { ThemeType, grommet } from 'grommet/themes';
import { deepMerge } from 'grommet/utils';
import { TextArea } from 'grommet';

export const Theme = deepMerge(grommet, <ThemeType>{
    global: {
        font: {
            /*family: 'Garamond' too hard to read a lot of text*/
        },
        colors: {
			/*brand: '#6cc2da'*/
			brand: '#6FFFB0'
        }
    },
    textInput: {
        extend: `
            background: white
        `
    },
    textArea: {
        extend: `
            background: white
        `
    },
    button: {
        border: {
            color: 'dark-1',
            radius: '0px'
        }
    },
    layer: {
        background: 'white'
	},
	heading: {
		level: {
			1: {
				font: {
					family: 'Garamond'
				}
			},
			2: {
				font: {
					family: 'Garamond'
				}
			},
			3: {
				font: {
					family: 'Garamond'
				}
			}
		}
	}
});
